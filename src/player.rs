/// file : player.rs
/// author : Maxime Rohmer (from Herbet Wolverson book)
/// version : 0.1.0
/// date : 26/04/2022
/// brief : File that contains all the Player related stuff

use crate::prelude::*;

pub struct Player {
    pub position: Point
}
impl Player{
    /// Construtor
    pub fn new(position:Point) -> Self{
        Self{
            position
        }
    }

    /// Render Logic
    pub fn render(&self,ctx: &mut BTerm) {
        ctx.set(
            self.position.x,
            self.position.y,
            WHITE,
            BLACK,
            to_cp437('@'),
        );
    }

    /// Player Movement Logic
    pub fn update(&mut self,ctx: &mut BTerm, map:&Map){
        if let Some(key) = ctx.key{
            let delta = match key{
                VirtualKeyCode::Left => Point::new(-1,0),
                VirtualKeyCode::Right => Point::new(1,0),
                VirtualKeyCode::Up => Point::new(0,-1),
                VirtualKeyCode::Down => Point::new(0,1),
                _ => Point::zero()
            };
            let new_position = self.position + delta;
            if map.can_enter_tile(new_position) {
                self.position = new_position;
            }
        }
    }
}