/// file : map.rs
/// author : Maxime Rohmer (from Herbet Wolverson book)
/// version : 0.1.0
/// date : 26/04/2022
/// brief : File that contains all the map related stuff

use crate::prelude::*;
const NUM_TILES: usize = (SCREEN_WIDTH * SCREEN_HEIGHT) as usize;

#[derive(Copy, Clone, PartialEq)]
pub enum TileType {
    Wall,
    Floor,
}

pub struct Map {
    pub tiles: Vec<TileType>,
}
pub fn map_idx(x: i32, y: i32) -> usize {
    ((y * SCREEN_WIDTH) + x)as usize
}
impl Map {
    /// Constructor
    pub fn new() -> Self{
        Self {
            tiles: vec![TileType::Floor; NUM_TILES],
        }
    }

    /// Bounds Logic
    pub fn in_bounds(&self,point : Point) -> bool{
        point.x >=0 && point.x < SCREEN_WIDTH 
        && point.y >= 0 && point.y < SCREEN_HEIGHT
    }
    pub fn can_enter_tile(&self,point: Point) -> bool {
        self.in_bounds(point) 
        && self.tiles[map_idx(point.x, point.y)] == TileType::Floor
    }
    pub fn  try_idx(&self, point : Point) -> Option<usize> {
        if !self.in_bounds(point){
            None
        }else{
            Some(map_idx(point.x,point.y))
        }
    }
    
    /// Render Logic
    pub fn render(&self, ctx: &mut BTerm){
        for y in 0..SCREEN_HEIGHT{
            for x in 0..SCREEN_WIDTH {
                let idx = map_idx(x,y);
                match self.tiles[idx] {
                    TileType::Floor =>{
                        ctx.set(x,y, YELLOW,BLACK,to_cp437('.'));
                    }
                    TileType::Wall =>{
                        ctx.set(x,y, GREEN, BLACK,to_cp437('#'));
                    }
                }
            }
        }
    }
}

