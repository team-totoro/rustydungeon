# RustyDungeon
## Design Document
Rohmer Maxime 26/04/2022 
<maxluligames@gmail.com>

### Disclaimer
This is not my own projec, its only my interpretation of the exercice from "Hands-on Rust Effective Learning through 2D Game Development and Play (Herbert Wolverson)" where you learn to create a simple RogueLike dungeon crawler.

### Short Description
A dungeon crawler with procedurally generated levels, monsters of increasingly difficulty, and turn-based movements.

### Story
The hero's hometown is suffering from a plague of monsters, Weilling up from the deep, they can seem unstoppable.Legend tells of the Amulet of Yala - Yet Another Lost Amulet - that can be used to stem the tide. After a long night at the tavern, the hero promises to dave the day - and set forth into the dungeon.

### Basic Game Loops
1. Enter dungeon level.
2. Explore, revealing the map.
3. Encounter ennemies whom the player flights or flees from.
4. Find power-ups and use the to strengthen the player
5. Locate the exit to the level - go to 1

### MVP
1. Create a basic dungeon map
2. Place the player and let them walk around
3. Spawn monsters, draw them, and let the player kill them by walking into them.
4. Add health and a combat system that uses it.
5. Add healing potions.
6. Display a "game over" screen when the player dies.
7. Add the Amulet of Yala to the level and let the player win by reaching it.

### MVP ++
1. Add Fields-Of-View.
2. Add more interesting dungeon designs.
3. Add some dungeons themes.
4. Add multiple layers to the dungeon, with the Amulet on the last one.
5. Add varied weapons to the game.
6. Move to a data-driven design for spawning ennemies.
7. Consider some visual effects to make combat more visceral.
8. Consider keeping score.
